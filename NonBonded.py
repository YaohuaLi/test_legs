from __future__ import division
import numpy as np
import hoomd
from hoomd import md
from numpy import linalg as la
from Loggable import Loggable

class LJRepulsive(Loggable):
    def __init__(self, log_list=None):

        super(LJRepulsive, self).__init__(log_list)
        self.log_values = ['pair_table_energy']

        self.epsilon = [1, 1, 1, 1, 1, 1, 1]

        self.sigma = [1, 0.5, 0.5, 0.5, 0.5, 0.5, 1]
        self.names = ['X', 'C', 'D', 'P', 'H', 'qPp', 'qPm']

        self.lj_repulsive_pair = None

    def set_lj_repulsive(self, neighbor_list, system, table_cd=False, helix=False):
        cut = 2
        self.lj_repulsive_pair = hoomd.md.pair.table(width=1000, nlist=neighbor_list)
        self.add_to_logger()
        for t1 in system.particles.types:
            for t2 in system.particles.types:
                t1 = str(t1)
                t2 = str(t2)
                if t1 == 'C' and t2 == 'C':
                    self.lj_repulsive_pair.pair_coeff.set(str(t1), str(t2), rmin=10e-5, rmax=2, func=LJRepulsive_pair,
                                                          coeff=dict(sigma=2, epsilon=1))
                elif t1 == 'D' and t2 == 'D':
                    self.lj_repulsive_pair.pair_coeff.set(str(t1), str(t2), rmin=10e-5, rmax=2, func=LJRepulsive_pair,
                                                          coeff=dict(sigma=2, epsilon=1))
                elif t1 == 'X' and t2 in self.names or t2 == 'X' and t1 in self.names:
                    self.lj_repulsive_pair.pair_coeff.set(str(t1), str(t2), rmin=10e-5, rmax=2, func=LJRepulsive_pair,
                                      coeff=dict(sigma=2,
                                                 epsilon=np.sqrt(self.epsilon[self.names.index(t1)] *
                                                                  self.epsilon[self.names.index(t2)])))
                elif (t1 == 'C' and t2 == 'D' or t1 == 'D' and t2 == 'C') and table_cd:
                    self.lj_repulsive_pair.set_from_file(str(t1), str(t2), filename='table_potential.txt')

                elif t1 == 'H' and t2 == 'H' and helix:
                    self.lj_repulsive_pair.pair_coeff.set(str(t1), str(t2), rmin=10e-5, rmax=2, func=H_potential,
                                                          coeff=dict(sigma=1, epsilon=1))


                elif t1 in self.names and t2 in self.names:
                    self.lj_repulsive_pair.pair_coeff.set(str(t1), str(t2), rmin=10e-5, rmax=2, func=LJRepulsive_pair,
                                      coeff=dict(sigma=(self.sigma[self.names.index(t1)] +
                                                        self.sigma[self.names.index(t2)]) / 2,
                                                 epsilon=np.sqrt(self.epsilon[self.names.index(t1)] *
                                                                  self.epsilon[self.names.index(t2)])))

                else:
                    self.lj_repulsive_pair.pair_coeff.set(str(t1), str(t2), rmin=10e-5, rmax=2, func=LJRepulsive_pair,
                                                          coeff=dict(sigma=0, epsilon=0))

        return self.lj_repulsive_pair


class Yukawa(Loggable):

    def __init__(self, log_list=None, debye=1, total_charge=None):
        super(Yukawa, self).__init__(log_list)
        self.log_values = ['pair_yukawa_energy']
        self.lb = .7
        self.sigma = [4, 0.5, 0.5, 0.5, 0.5, 0.5, 1]
        self.names = ['X', 'C', 'D', 'P', 'H', 'qPp', 'qPm']
        self.charge = [0, 0, 0, 0, 0, 1, -1]
        self.kappa = 1/debye
        self.yukawa = None
        self.total_charge = total_charge


    def set_yukawa(self, neighbor_list, system):

        yuk = hoomd.md.pair.yukawa(r_cut=3 / self.kappa, nlist=neighbor_list)
        self.add_to_logger()

        if self.total_charge is not None:
            count = 0
            for t in system.particles:
                t = str(t.type)
                if t == 'qPm':
                    count += 1
            self.charge[self.names.index('qPm')] = self.total_charge / count

        for t1 in system.particles.types:
            for t2 in system.particles.types:
                t1 = str(t1)
                t2 = str(t2)
                if t1 in self.names and t2 in self.names:
                    sigma = .5 * (self.sigma[self.names.index(t1)] + self.sigma[self.names.index(t2)])
                    q1 = self.charge[self.names.index(t1)]
                    q2 = self.charge[self.names.index(t2)]
                    eps = q1 * q2 * self.lb * np.exp(self.kappa * sigma) / (1/self.kappa + sigma)
                    #eps = eps * 100
                    yuk.pair_coeff.set(t1, t2, epsilon=eps, kappa=self.kappa)
                else:
                    yuk.pair_coeff.set(t1, t2, epsilon=0, kappa=self.kappa)


class LJSpecial(Loggable):
    def __init__(self, log_list=None, energy=10):

        super(LJSpecial, self).__init__(log_list)

        self.log_values = ['pair_lj_energy']

        self.names = ['X', 'C', 'D', 'P', 'H', 'qPm', 'qPp']

        self.epsilon = [1, 1, 1, 1, 1, 1, 1]

        self.sigma = [4, 0.5, 0.5, .5, .5, .5, 1]

        self.lj_pair = None

        self.energy = energy

    def set_lj_special(self, neighbor_list, system):
        cut = 3
        self.lj_pair = hoomd.md.pair.lj(r_cut=cut, nlist=neighbor_list)
        self.add_to_logger()
        for t1 in system.particles.types:
            for t2 in system.particles.types:
                t1 = str(t1)
                t2 = str(t2)
                if t1 == 'C' and t2 == 'D' or t1 == 'D' and t2 == 'C':
                    self.lj_pair.pair_coeff.set(str(t1), str(t2), epsilon=self.energy, sigma=.5)
                else:
                    self.lj_pair.pair_coeff.set(str(t1), str(t2), epsilon=0, sigma=1)


def LJRepulsive_pair(r,rmin, rmax, sigma, epsilon):

    if r < sigma:
        V = epsilon * ((sigma / r) ** 12 - 1)
        F = epsilon * (sigma/r) ** 13
    else:
        V = 0
        F = 0
    return (V,F)


def quad(r, sigma, epsilon):
    if r < (sigma + .25) and r > (sigma - .25):
        V = 16 * epsilon * (r - 1) ** 2 - epsilon
        F = - (16 * epsilon * (2 * r - 2))
    else:
        V = 0
        F = 0
    return (V, F)


def H_potential(r, rmin, rmax, sigma, epsilon):
    if r > .5:
        return quad(r, sigma, epsilon)
    else:
        return LJRepulsive_pair(r,0,0, .5, 1)

