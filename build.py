import sys
import os
from hoomd import *
from hoomd import md
from hoomd import deprecated as hdepr
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


base1 = np.array([0, 1, 0])
base2 = np.array([np.sqrt(3)/2.0, -0.5, 0])


hexamer=[]

for i in range(-2, 3):
    for j in range(-2, 3):
        position = base1*i + base2*j
        if position[1]-0.5*position[0] < 2.1 and position[1]-0.5*position[0]>-2.1:
            hexamer.append(tuple(position))

hexamer.append((5,0,0))
hexamer.append((-5,0,0))
coord_A= np.array(hexamer)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
xs = coord_A[:, 0]
ys = coord_A[:, 1]
zs = coord_A[:, 2]
ax.scatter(xs, ys, zs)
plt.show()
