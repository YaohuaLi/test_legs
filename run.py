from  __future__ import division
import sys
sys.path.append('/home/yaohua/Downloads/repos/legs')

from Mer import Mer
from Arm import Arm
from BasicSV40Body import BasicSV40Body
from Leg import Leg
from DNATorroid import DNATorroid
import copy as cp
from Solution import Solution
from Solution import FiveCoord
from Solution import Lattice
from Simulation import Simulation
from ChargedPolymer import ChargedPolymer
from SphericalTemplate import SphericalTemplate
from HaganArm import HaganArm
from HelixLeg import HelixLeg
from PduaBody import PduaBody
import numpy as np


body = PduaBody()
print body.binding_sites
arm = HaganArm()
leg = HelixLeg()
mer = Mer(body, arm, leg)
mer.shift([0,0,10])
mers = [mer]

temp = SphericalTemplate(1)

chains = [temp]

s = Solution(mers, chains)
sys = s.create_system()
# s.dump_map(sys)

sim = Simulation(sys, energy=8, helix=0)
sim.set_log_period(1e3)
sim.set_dump_period(1e2)
sim.set_dt(.003)
sim.run(1e5)
