from  __future__ import division
import sys
sys.path.append('/home/yaohua/Downloads/repos/legs')

from Mer import Mer
from Arm import Arm
from BasicSV40Body import BasicSV40Body
from Leg import Leg
from DNATorroid import DNATorroid
import copy as cp
from Solution import Solution
from Solution import FiveCoord
from Solution import Lattice
from Simulation import Simulation
from ChargedPolymer import ChargedPolymer
from SphericalTemplate import SphericalTemplate
from HaganArm import HaganArm
from HelixLeg import HelixLeg
from PduaBody import PduaBody
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

coord_A = np.genfromtxt('cg_coord.txt', delimiter='\t')

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
xs = coord_A[:, 0]
ys = coord_A[:, 1]
zs = coord_A[:, 2]
ax.scatter(xs,ys,zs)
plt.show()
charges_A = np.genfromtxt('charge.txt')
r=4
ir=2
i=5
body_sites=[]
arm_sites = [[2.2749, 2.599, 1.9701], [-1.1134, 3.2696, 1.9701], [-3.3883, 0.67062, 1.9701],
                          [-2.2749, -2.599, 1.9701], [1.1134, -3.2696, 1.9701], [3.3883, -0.67062, 1.9701]]


for i in range(len(coord_A)):

    if list(coord_A[i]) not in arm_sites:
        body_sites.append(list(coord_A[i]))


a = list(coord_A[1])
hand_A = [[2.2749, 2.599, 1.9701],[-1.1134, 3.2696, 1.9701],[-3.3883, 0.67062, 1.9701],
    [-2.2749, -2.599, 1.9701], [ 1.1134, -3.2696, 1.9701], [ 3.3883, -0.67062, 1.9701]]

charges=np.genfromtxt('cg_coord.txt')



